// Exercise2
package main

import (
	"fmt"
	"strconv"

	"./temp" // 自作パッケージであるtemp呼び出し。importとしてはフォルダ名を指定してあげる。
)

func main() {
	var beDivNum int = 20
	var divNum float64 = 0.002
	// 方法１：float64型をformat指定で標準出力するPrintfを使用する。
	// float64型をを出力するためには%gか、%vを使用する必要がある。\nは改行コード
	// tempパッケージからDivideCallを呼び出す。
	fmt.Printf("%g\n", temp.DivideCall(beDivNum, divNum))
	// 方法２：float64型をstring型にキャストして、Printlnで標準出力する。
	// 計算結果を変数resultに格納する。
	var result float64 = temp.DivideCall(beDivNum, divNum)
	// strconv packageのFormatFloat関数を用いて、float64をstring型にキャストし、その戻り値を標準出力する。
	fmt.Println(strconv.FormatFloat(result, 'f', 0, 64))
	// 方法３：そのまま出力
	fmt.Println(result)
}
