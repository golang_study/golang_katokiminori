// TempFunc
package temp // パッケージ名temp

// Excercise2で呼び出されるメソッドであるため先頭大文字。
func DivideCall(beDivNum int, divNum float64) float64 {
	// dividingメソッドの戻り値をそのまま使用する。
	return dividing(beDivNum, divNum)
}

// 割り算をする関数dividing
func dividing(beDivNum int, divNum float64) float64 {
	// beDivNumはint型であり、divNumのfloat64型とは異なるため、型の変換が必要。
	// beDivNumをfloat64型に変換(キャスト)する。
	var result = float64(beDivNum) / divNum
	// 計算結果をreturnする
	return result
}
