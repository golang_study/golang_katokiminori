// Execise5
package main

import (
	"fmt"
)

func main() {
	// 要素数10個の配列を宣言
	var result [10]int

	//10回分の繰り返し処理
	for i := 1; i <= 10; i++ {
		/**
		  配列の0番目に繰り返し処理のi(=1)を設定
		  配列の1番目に繰り返し処理のi(=2)を設定
		  配列のi-1番目に繰り返し処理のiを設定
		**/
		result[i-1] = i
	}
	// 作成した配列を標準出力
	fmt.Println(result)
}
