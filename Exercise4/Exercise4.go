// Exercise4
package main

import (
	"fmt"
)

func main() {
	// ループ処理 i=1を初期値とし、i <= 100がtrueである限りiをインクリメントする
	for i := 1; i <= 100; i++ {
		// 奇数判定 2で割った時の余りが0でないものが奇数
		if i%2 != 0 {
			// 奇数出力
			fmt.Println(i)
		}
	}
}
