// Exercise3
package main

import (
	"fmt"
)

func main() {
	// 出力用数値型変数定義
	var result int = 0
	// ループ処理 i=1を初期値とし、i <= 100がtrueである限りiをインクリメントする
	for i := 1; i <= 100; i++ {
		// 加算処理 result = result + iでも良い
		result += i
	}
	// 算出結果出力
	fmt.Println(result)
}
