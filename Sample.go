// Sample
package main

import (
	"fmt"
)

func main() {
	var array [5]string = [...]string{"おじさん", "マイメロ", "競馬", "パチスロ", "パチンコ"}
	/*
		Slice型の変数宣言
		宣言+初期化処理を1行で行う
		var 変数名[]型 = array[①最初のindex番号:②最後のindex番号-1]
	*/
	var slice []string = array[0:2]
	fmt.Println(slice)
}
