// Exercise1
package main

import (
	"fmt"
	"strconv"
)

func main() {
	var beDivNum int = 20
	var divNum float64 = 0.002
	// 方法１：float64型をformat指定で標準出力するPrintfを使用する。
	// float64型をを出力するためには%gか、%vを使用する必要がある。\nは改行コード
	fmt.Printf("%g\n", dividing(beDivNum, divNum))
	// 方法２：float64型をstring型にキャストして、Printlnで標準出力する。
	// 計算結果を変数resultに格納する。
	var result float64 = dividing(beDivNum, divNum)
	// strconv packageのFormatFloat関数を用いて、float64をstring型にキャストし、その戻り値を標準出力する。
	fmt.Println(strconv.FormatFloat(result, 'f', 0, 64))
	// 方法３：そのまま出力
	fmt.Println(result)
}

// 割り算をする関数dividing
func dividing(beDivNum int, divNum float64) float64 {
	// beDivNumはint型であり、divNumのfloat64型とは異なるため、型の変換が必要。
	// beDivNumをfloat64型に変換(キャスト)する。
	var result = float64(beDivNum) / divNum
	// 計算結果をreturnする
	return result
}
