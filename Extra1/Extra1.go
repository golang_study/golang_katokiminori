// Extra1
package main

import (
	"fmt"
	"sort"
	"strconv"
)

func main() {
	// string型のSlice宣言
	var str_slice []string
	// float64型のSlice宣言
	var flt_slice []float64
	// int型のSlice宣言
	var reserve_slice []int
	for i := 1; i <= 100; i++ {
		// switch文でやる場合
		switch {
		case i%15 == 0: // 15で割り切れる場合
			reserve_slice = append(reserve_slice, i)
		case i%3 == 0: // 3で割り切れる場合
			str_slice = append(str_slice, strconv.Itoa(i)+"個")
		case i%5 == 0: // 5で割り切れる場合
			flt_slice = append(flt_slice, float64(i)/10)
		}
		/**
		if文でやる場合
		f i%15 == 0 {
			reserve_slice = append(reserve_slice, i)
		} else if i%3 == 0 {
			str_slice = append(str_slice, strconv.Itoa(i)+"個")
		} else if i%5 == 0 {
			flt_slice = append(flt_slice, float64(i)/10)
		}
		**/
	}
	// 降順ソート(呼び出しがとても面倒)
	sort.Sort(sort.Reverse(sort.IntSlice(reserve_slice)))

	// 繰り返し処理：Sliceの要素数分だけループ処理を行う。要素数が固定でないので,lenで長さを求める必要がある
	for i := 0; i < len(str_slice); i++ {
		fmt.Println(str_slice[i])
	}
	for i := 0; i < len(flt_slice); i++ {
		fmt.Println(flt_slice[i])
	}
	for i := 0; i < len(reserve_slice); i++ {
		fmt.Println(reserve_slice[i])
	}
}
