# Project Title
GO_WORK Project
## Overview
Go言語勉強会のための課題解答やSample格納用リポジトリ

## Description
Sample.go:勉強会時に使用する簡単なSampleファイル
Exercize1〜n:演習解答ファイル格納フォルダ

## Programing Language
Golang

## Development tool
LiteIDE

## Others

### 暇つぶしに実装したDocker環境について(goApiStudy/Dockerfile,docker-compose.yml)
Docker実装にあたって、Daemon化は現時点でサポートされていない。
services/hoge.serviceを登録して回避する。dockerコンテナ起動の際にはcomposeを用いる。
```
#dockerコンテナ起動
docker-compose up -d

```
