// Exercise6
package main

import (
	"fmt"
)

func main() {
	// int型のSlice型の変数resultを宣言
	// 初期値なし、要素数0
	var result []int = []int{}
	// 1~10の繰り返し処理
	for i := 1; i <= 10; i++ {
		// 奇数のとき
		if i%2 == 1 {
			// Slice型のresultにappendを追加する
			result = append(result, i)
		}
	}
	// resultを標準出力
	fmt.Println(result)
}
